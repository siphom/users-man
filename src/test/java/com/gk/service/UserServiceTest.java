package com.gk.service;

import com.gk.domain.User;
import com.gk.dto.UserDTO;
import com.gk.repository.UserRepository;
import com.gk.repository.UserSessionRepository;
import com.gk.security.JwtTokenUtil;
import com.gk.translator.Translator;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.jws.soap.SOAPBinding;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Mock
    private UserRepository userRepo;

    @Mock
    private UserSessionRepository userSessionRepo;

    @Mock
    private Translator translator;

    @Mock
    private UserService userService;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    /**
     * Sets .
     */
    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void findAllUsersWithValidSession(){
        when(userService.findAllUsersWithValidSession()).thenReturn(Arrays.asList(getUserDTO()));
        when(userRepo.findAll()).thenReturn(Arrays.asList(getUser()));
        List<User> users = userRepo.findAll();
        assertEquals(1,users.size());
    }

    @Test
    public void logout(){
        when(userService.logout(any(String.class))).thenReturn(false);
    }

    @Test
    public void toUser(){
        Translator translator = new Translator();
        UserDTO dto = getUserDTO();
        Assert.assertNotNull(translator.toUser(dto));
    }

    @NotNull
    private UserDTO getUserDTO() {
        UserDTO dto = new UserDTO();
        dto.setEmail("sipamash@gmail.com");
        dto.setFirstName("Sipho");
        return dto;
    }

    @Test
    public void toUserDTO(){
        Translator translator = new Translator();
        User user = getUser();
        Assert.assertNotNull(translator.toUserDTO(user));
    }

    @NotNull
    private User getUser() {
        User user = new User();
        user.setEmail("sipamash@gmail.com");
        user.setFirstName("Sipho");
        return user;
    }

    @Test
    public void toUserDTO1(){
        Translator translator = new Translator();
        User user = getUser();
        Assert.assertNotNull(translator.toUser(user, getUserDTO()));
    }
}