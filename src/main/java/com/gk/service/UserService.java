package com.gk.service;

import com.gk.ApplicationException;
import com.gk.domain.User;
import com.gk.domain.UserSession;
import com.gk.dto.UserDTO;
import com.gk.repository.UserRepository;
import com.gk.repository.UserSessionRepository;
import com.gk.security.JwtTokenUtil;
import com.gk.translator.Translator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserService {

    @Autowired
    public UserRepository userRepo;
    private Translator translator = new Translator();

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    public UserSessionRepository userSessionRepo;

    @Value("${jwt.token-expire-minutes}")
    private int tokenExpireMinutes;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Transactional
    public UserDTO create(UserDTO dto){
        User user = userRepo.findByIdNo(dto.getIdNo());
        if (user != null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_USER_ALREADY_EXIST, "User already exist");
        }
        return translator.toUserDTO(
                userRepo.save(translator.toUser(dto))
        );
    }

    @Transactional
    public UserDTO update(UserDTO dto, Long id){
        //User user = userRepo.findByIdNo(id);
        User user = userRepo.findOne(id);
        if (user == null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_USER_NOT_FOUND, "User not found while attempting to update");
        }

        return translator.toUserDTO(
                userRepo.save(translator.toUser(user, dto))
        );
    }

    @Transactional
    public UserDTO search(Long id){
        //User user = userRepo.findByIdNo(id);
        User user = userRepo.findOne(id);
        if (user == null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_USER_NOT_FOUND, "User not found");
        }
        return translator.toUserDTO(
                user
        );
    }

    @Transactional
    public List<UserDTO> findAllUsersWithValidSession(){
        List<User> listOfUsers = userRepo.findAll();
        List<UserDTO> listOfUserDTO = new ArrayList<>();
        listOfUsers.forEach(user -> {
            UserSession userSession = userSessionRepo.findByTokenAndStatus(user.getUsername(), UserSession.Status.Active);
            if (userSession != null && userSession.getStatus().equals(UserSession.Status.Active)){
                listOfUserDTO.add(translator.toUserDTO(user));
            }
        });
        return listOfUserDTO;
    }

    public String getToken(UserDTO dto){
        String token = jwtTokenUtil.generateToken(dto.getUsername(), dto.getPassword(), secret);
        logger.debug("Token generated for username: {} ", dto.getUsername());
        UserSession session = new UserSession();
        session.setExpireDateTime(LocalDateTime.now().plusMinutes(tokenExpireMinutes));
        session.setStatus(UserSession.Status.Active);
        session.setToken(token);
        session.setUsername(dto.getUsername());
        userSessionRepo.save(session);
        return token;
    }

    public boolean logout(String token){
        UserSession userSession = userSessionRepo.findByToken(token);
        if (userSession != null){
            String username = jwtTokenUtil.getUsernameFromToken(token);
            if (username != null && userSession.getUsername().equals(username)){
                userSession.setExpireDateTime(LocalDateTime.now().minusSeconds(30));
                userSession.setStatus(UserSession.Status.Expired);
                userSessionRepo.save(userSession);
                return true;
            }
        }
        return false;
    }

    /**
     * This scheduler monitors all active tokens and expire them if life span elapsed
     */
    @Scheduled(fixedDelay = 30000, initialDelay = 30000)  // every 5 seconds, 30 second initial delay
    public void expireTokens() {
        List<UserSession> userSessionList = userSessionRepo.findByStatus(UserSession.Status.Active);
        userSessionList.forEach(userSession -> {
            if (jwtTokenUtil.isTokenExpired(userSession.getToken())){
                userSession.setStatus(UserSession.Status.Expired);
                userSessionRepo.save(userSession);
                logger.debug("Token for username: {} has expired", userSession.getUsername());
            }
        });
    }
}