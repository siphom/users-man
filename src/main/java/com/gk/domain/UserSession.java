package com.gk.domain;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "USER_SESSION", uniqueConstraints = @UniqueConstraint(columnNames = {"TOKEN"}))
@Access(AccessType.FIELD)
@SequenceGenerator(name = "seq_gk", sequenceName = "seq_user_session", allocationSize = 1, initialValue = 1)
public class UserSession extends AbstractPersistable<Long>{
    private String token;
    private LocalDateTime expireDateTime;
    private String username;

    public enum Status{
        Active,
        Expired
    }

    @Enumerated(EnumType.STRING)
    private UserSession.Status status;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getExpireDateTime() {
        return expireDateTime;
    }

    public void setExpireDateTime(LocalDateTime expireDateTime) {
        this.expireDateTime = expireDateTime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}