package com.gk.security;

/**
 * Created by Sipho on 2019/02/04
 */
public class Headers {
  private String token;

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }
}
