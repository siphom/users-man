package com.gk.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This class configures endpoints that should not be authenticated
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Autowired
	private UserDetailsService userDetailsService;

	public WebSecurityConfig() {
		//need
	}

	/**
	 * Initialise {@link JwtAuthenticationTokenFilter}
	 * @return {@link JwtAuthenticationTokenFilter}
	 */
    @Bean
    public JwtAuthenticationTokenFilter authenticationTokenFilterBean(){
        return new JwtAuthenticationTokenFilter();
    }

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.csrf().disable()
			.exceptionHandling().authenticationEntryPoint(unauthorizedHandler)
			.and()
			.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
			.and()
			.authorizeRequests()
				.antMatchers("/health").permitAll()
				.antMatchers("/info").permitAll()
				.antMatchers("/trace").permitAll()
				.antMatchers("/mappings").permitAll()
				.antMatchers("/metrics").permitAll()


				.antMatchers("/v2/api-docs*/**").permitAll()
				.antMatchers("/swagger-resources*/**").permitAll()
				.antMatchers("/swagger-ui.html*/**").permitAll()
				.antMatchers("/swagger-ui.html/").permitAll()
				.antMatchers("/swagger-ui.html").permitAll()
				.antMatchers("/webjars*/**").permitAll()

				.antMatchers("/api/login").permitAll()
				.antMatchers("/csrf").permitAll()
				.antMatchers("/springfox").permitAll()

				.antMatchers(HttpMethod.OPTIONS, "/**").permitAll()
				.antMatchers("/**").authenticated()
			.and()
			.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class)
			.headers().cacheControl();


	}

	/**
	 * Configure authentication
	 *
	 * @param authenticationManagerBuilder the authentication manager builder
	 * @throws Exception the exception
	 */
	@Autowired
	public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
			authenticationManagerBuilder
							.userDetailsService(this.userDetailsService);
	}

	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		return username -> {
            Collection<? extends GrantedAuthority> authorities  = new ArrayList<>();
            return new User(username, "",  authorities);
        };
	}
}