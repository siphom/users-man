package com.gk.security;

import com.gk.ApplicationException;
import com.gk.domain.UserSession;
import com.gk.repository.UserSessionRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This class generates token
 */
@Service
public class JwtTokenUtil implements Serializable {

    private static final long serialVersionUID = -3301605591108950415L;
    private transient Logger log = LoggerFactory.getLogger(JwtTokenUtil.class);
    static final String CLAIM_KEY_USERNAME = "USERNAME_ID";
    static final String CLAIM_KEY_PASSWORD = "PASSWORD";
    static final String CLAIM_KEY_SECRET_CODE = "secret";

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.token-expire-minutes}")
    private int tokenExpireMinutes;

    @Autowired
    private UserSessionRepository userSessionRepo;

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getUsernameFromToken(String authToken){
        String username = null;
        try {
            final Claims claims = getClaimsFromToken(authToken);
            if (claims != null){
                username = claims.get(CLAIM_KEY_USERNAME).toString();

                UserSession userSession = userSessionRepo.findByToken(authToken);
                if (userSession.getExpireDateTime().isAfter(LocalDateTime.now())){
                    userSession.setExpireDateTime(LocalDateTime.now().plusMinutes(tokenExpireMinutes));
                    userSessionRepo.save(userSession);
                } else if(userSession.getStatus().equals(UserSession.Status.Active)){//just in case if token is active and valid time is before now
                    userSession.setStatus(UserSession.Status.Expired);
                    userSessionRepo.save(userSession);
                }
            }
        } catch (Exception e) {
            username = null;
            log.debug(e.getMessage(), e);
        }
        return username;
    }

    private Claims getClaimsFromToken(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser()
                    .setSigningKey(secret.getBytes())
                    .parseClaimsJws(token)
                    .getBody();
        } catch (Exception e) {
            log.debug(e.getMessage(), e);
        }
        return claims;
    }

    /**
     * Validate token
     *
     * @param token       the token
     * @param userDetails the user details
     * @return the boolean validateToken response
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
    	User user = (User) userDetails;
        final String username = getUsernameFromToken(token);
        return username.equals(user.getUsername());
    }

    public String generateToken(String username, String password, String secret) {
        Map<String, Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME, username);
        claims.put(CLAIM_KEY_PASSWORD, password);
        claims.put(CLAIM_KEY_SECRET_CODE, secret);
        return generateToken(claims);
    }

    String generateToken(Map<String, Object> claims) {
        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret.getBytes())
                .setExpiration(new Date(System.currentTimeMillis() + (3 * 60 * 1000)))
                .compact();
    }

    /**
     * If token expired return true else return false and increase token life span
     * @param authToken
     * @return
     */
    public boolean isTokenExpiredAndTokenLifeSpanNotIncreased(String authToken) {
        final Claims claims = getClaimsFromToken(authToken);
        if (claims != null) {
            UserSession userSession = userSessionRepo.findByToken(authToken);
            if (userSession == null){
                throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_TOKEN_EXPIRED, "Token expired");
            }

            if (userSession.getExpireDateTime().isBefore(LocalDateTime.now())){
                return true;
            } else {
                userSession.setExpireDateTime(LocalDateTime.now().plusMinutes(tokenExpireMinutes));
                userSessionRepo.save(userSession);
                log.debug("Token for username {} life span increased with {} minutes", userSession.getUsername(), tokenExpireMinutes);
            }
        }
        return false;
    }

    public boolean isTokenExpired(String authToken) {
        final Claims claims = getClaimsFromToken(authToken);
        if (claims != null) {
            UserSession userSession = userSessionRepo.findByToken(authToken);
            if (userSession == null){
                throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_TOKEN_EXPIRED, "Token expired");
            }

            if (userSession.getExpireDateTime().isBefore(LocalDateTime.now())){
                return true;
            }
        }
        return false;
    }
}