package com.gk.security;

import com.gk.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Jwt athentication token filter
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Value("${jwt.header}")
    private String tokenHeader;

    @Value("${jwt.secret}")
    private String secret;

    public JwtAuthenticationTokenFilter() {
        //sonarcube/sonarlint requires that there should be a public default construct physically defined
    }

    /**
     * JwtAuthenticationTokenFilter constructor
     *
     * @param jwtTokenUtil       the jwt token util
     * @param userDetailsService the user details service
     */
    public JwtAuthenticationTokenFilter(JwtTokenUtil jwtTokenUtil, UserDetailsService userDetailsService) {
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authToken = request.getHeader(this.tokenHeader);
        if (authToken != null && !authToken.isEmpty()){
            String username = jwtTokenUtil.getUsernameFromToken(authToken);
            if (jwtTokenUtil.isTokenExpiredAndTokenLifeSpanNotIncreased(authToken)){
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_TOKEN_EXPIRED, "Token expired");
            } else if (username != null) {

                // It is not compelling necessary to load the use details from the database. You could also store the information
                // in the token and read it from it. It's up to you ;)
                UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
                // For simple validation it is completely sufficient to just check the token integrity. You don't have to call
                // the database compellingly. Again it's up to you ;)

                if (jwtTokenUtil.validateToken(authToken, userDetails)) {
                    UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                    authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                    response.setStatus(HttpServletResponse.SC_OK);
                } else if (logger.isDebugEnabled()){
                    logger.debug("JWT Token validation failed with username:" + username);
                }
                request.getRequestDispatcher( request.getRequestURI()).forward(request,response);
            } else {
                logger.debug("Invalid username");
                response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            }
        } else {
            chain.doFilter(request, response);
        }
    }
}