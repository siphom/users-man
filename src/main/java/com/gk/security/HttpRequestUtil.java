package com.gk.security;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Sipho on 2019/02/04
 */
public class HttpRequestUtil {
  private static final String X_AUTH_HEADER = "X-Auth-Token";

  private HttpRequestUtil(){
    //private constructor to hide the implicit public one
  }

  /**
   * Get request headers
   * @param request {@Link HttpServletRequest}
   * @return {@Link Map}
   */
  private static Map<String, String> getRequestHeaders(HttpServletRequest request) {
    Map<String, String> headers = new HashMap<>();
    Enumeration headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String key = (String) headerNames.nextElement();
      String value = request.getHeader(key);
      headers.put(key, value);
    }
    return headers;
  }

  /**
   * Get all headers
   * @param request {@Link HttpServletRequest}
   * @return {@Link Headers}
   */
  public static Headers getAllHeaders(HttpServletRequest request){
    Map mappedHeaders = getRequestHeaders(request);
    Headers headers = new Headers();
    mappedHeaders.forEach((key,value) -> setHeaders(headers,key, value));
    return headers;
  }

  /**
   * Set headers and if a specific header value is required add case for it
   * @param headers
   * @param key
   * @param value
   * @return
   */
  private static Headers setHeaders(Headers headers, Object key, Object value){
    switch (key.toString().toUpperCase()){
      case X_AUTH_HEADER:
        headers.setToken(value.toString());
        break;
      default:
    }
    return headers;
  }
}