package com.gk;

import com.gk.domain.User;
import com.gk.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class LoadTestData implements ApplicationRunner {

    private static Logger logger = LoggerFactory.getLogger(LoadTestData.class);

    @Autowired
    private UserRepository userRepo;

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        User user = new User();
        user.setIdNo(12345L);
        user.setUsername("admin");
        user.setPassword("admin");
        user.setEmail("admin@admin.com");
        user.setFirstName("admin user");
        user.setLastName("admin");
        user.setRace("M");
        user.setMobileNumber("08211111111");
        user.setRole(User.Role.ADMIN_ROLE);
        userRepo.save(user);
        logger.debug("Done loading test user");
    }
}
