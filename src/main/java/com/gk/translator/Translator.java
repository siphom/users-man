package com.gk.translator;

import com.gk.domain.User;
import com.gk.dto.UserDTO;

public class Translator {

    //user
    public User toUser(UserDTO dto){
        User user = new User();
        setUser(dto, user);
        return user;
    }

    public User toUser(User emp, UserDTO dto){
        setUser(dto, emp);
        return emp;
    }

    private void setUser(UserDTO dto, User user) {
        user.setIdNo(dto.getIdNo());
        user.setEmail(dto.getEmail());
        user.setFirstName(dto.getFirstName());
        user.setIdNo(dto.getIdNo());
        user.setRace(dto.getRace());
        user.setUsername(dto.getUsername());
        user.setMobileNumber(dto.getMobileNumber());
        user.setLastName(dto.getLastName());
    }

    public UserDTO toUserDTO(User emp){
        UserDTO dto = new UserDTO();
        if (emp != null){
            dto.setEmail(emp.getEmail());
            dto.setFirstName(emp.getFirstName());
            dto.setIdNo(emp.getIdNo());
            dto.setId(emp.getId());
            dto.setRace(emp.getRace());
            dto.setMobileNumber(emp.getMobileNumber());
            dto.setLastName(emp.getLastName());
            dto.setUsername(emp.getUsername());
            dto.setId(emp.getId());
        }
        return dto;
    }
}
