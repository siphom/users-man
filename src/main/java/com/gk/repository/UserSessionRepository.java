package com.gk.repository;

import com.gk.domain.User;
import com.gk.domain.UserSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface UserSessionRepository extends JpaRepository<UserSession, Long>, JpaSpecificationExecutor<User>{
    UserSession findByToken(String token);
    UserSession findByTokenAndStatus(String token, UserSession.Status status);
    List<UserSession> findByStatus(UserSession.Status status);
}
