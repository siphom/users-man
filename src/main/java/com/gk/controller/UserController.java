package com.gk.controller;

import com.gk.ApplicationException;
import com.gk.domain.User;
import com.gk.domain.UserSession;
import com.gk.dto.UserDTO;
import com.gk.repository.UserRepository;
import com.gk.repository.UserSessionRepository;
import com.gk.security.JwtTokenUtil;
import com.gk.service.UserService;
import org.apache.http.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Response;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping(value = "${gk.app.api-prefix}")
public class UserController {

    @Autowired
    private UserService userService;

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserSessionRepository userSessionRepo;

    @Value("${jwt.token-expire-minutes}")
    private int tokenExpireMinutes;

    @PostMapping("/user")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO create(@Validated @RequestBody UserDTO dto)
    {
        return userService.create(dto);
    }

    @PutMapping("/user/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO update(@Validated @RequestBody UserDTO dto, @PathVariable(name = "id") Long id){
        return userService.update(dto, id);
    }

    @GetMapping("/user/{id}")
    public UserDTO search(@PathVariable(name = "id") Long id){
        return userService.search(id);
    }

    @GetMapping("/users")
    public List<UserDTO> findAllUsersWithValidSession(){
        return userService.findAllUsersWithValidSession();
    }

    @PostMapping("/login")
    public ResponseEntity login(@RequestBody UserDTO dto){
        if (dto.getUsername() == null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_USER_NOT_FOUND, "User does not found");
        }
        User user = userRepo.findByUsername(dto.getUsername());


        if (user == null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_USER_NOT_FOUND, "User does not found");
        } else{

        }

        return ResponseEntity.ok().header("X-Auth-Token", userService.getToken(dto)).build();
    }

    @PostMapping("/logout")
    public boolean logout(@RequestHeader(value = "X-Auth-Token") String token){
        if (token == null){
            throw new ApplicationException(ApplicationException.Type.client_error, ApplicationException.CODE_TOKEN_EXPIRED, "Token expired");
        }
        return userService.logout(token);
    }
}